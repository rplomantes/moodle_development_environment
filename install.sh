#!/bin/sh

#COLORS
# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan

sudo apt-get update
sudo apt-get upgrade -y

echo -e "$Cyan \n Installing Apache2 $Color_Off"
sudo apt-get install apache2 -y

echo -e "$Cyan \n Installing PHP & Requirements $Color_Off"

sudo apt install software-properties-common -y
sudo add-apt-repository ppa:ondrej/php -y
sudo apt update -y

sudo apt-get install php8.1 libapache2-mod-php8.1 php8.1-gd php8.1-mbstring -y
sudo apt-get install php8.1-curl php8.1-xml php8.1-soap php8.1-xmlrpc -y
sudo apt-get install php8.1-intl php8.1-zip php8.1-mysql -y

echo -e "$Cyan \n Installing MYSQL $Color_Off"

sudo apt-get install mysql-common mysql-server -y

echo -e "$Cyan \n Create database and user in MYSQL $Color_Off"
sudo mysql -e "CREATE DATABASE  moodle"
sudo mysql -e "CREATE USER moodleuser@localhost identified by 'Password'"
sudo mysql -e "GRANT ALL ON moodle.* to moodleuser@localhost"
sudo mysql -e "FLUSH PRIVILEGES"

echo -e "$Cyan \n Installing Moodledata $Color_Off"

sudo mkdir /var/moodledata
sudo chmod 777 /var/moodledata

echo -e "$Cyan \n Installing Moodle $Color_Off"

cd /var/www/html
sudo git clone --branch MOODLE_403_STABLE https://github.com/moodle/moodle.git

cd /var/www/html/moodle
sudo touch config.php
sudo echo "<?php  // Moodle configuration file" >> config.php
sudo echo " " >> config.php
sudo echo "unset(\$CFG);" >> config.php
sudo echo "global \$CFG;" >> config.php
sudo echo "\$CFG = new stdClass();" >> config.php
sudo echo " " >> config.php
sudo echo "\$CFG->dbtype    = 'mysqli';" >> config.php
sudo echo "\$CFG->dblibrary = 'native';" >> config.php
sudo echo "\$CFG->dbhost    = 'localhost';" >> config.php
sudo echo "\$CFG->dbname    = 'moodle';" >> config.php
sudo echo "\$CFG->dbuser    = 'moodleuser';" >> config.php
sudo echo "\$CFG->dbpass    = 'Password';" >> config.php
sudo echo "\$CFG->prefix    = 'mdl_';" >> config.php
sudo echo "\$CFG->dboptions = array (" >> config.php
sudo echo "'dbpersist' => 0," >> config.php
sudo echo "'dbport' => ''," >> config.php
sudo echo "'dbsocket' => ''," >> config.php
sudo echo "'dbcollation' => 'utf8mb4_0900_ai_ci'" >> config.php
sudo echo ");" >> config.php
sudo echo " " >> config.php
sudo echo "\$CFG->wwwroot   = 'http://localhost/moodle';" >> config.php
sudo echo "\$CFG->dataroot  = '/var/moodledata';" >> config.php
sudo echo "\$CFG->admin     = 'admin';" >> config.php
sudo echo "\$CFG->directorypermissions = 0777;" >> config.php
sudo echo "\$CFG->debug = E_ALL;" >> config.php
sudo echo "\$CFG->debugdisplay = 1;" >> config.php
sudo echo "\$CFG->langstringcache = 0;" >> config.php
sudo echo "\$CFG->cachetemplates = 0;" >> config.php
sudo echo "\$CFG->cachejs = 0;" >> config.php
sudo echo "\$CFG->perfdebug = 15;" >> config.php
sudo echo "\$CFG->debugpageinfo = 1;" >> config.php
sudo echo " " >> config.php
sudo echo "require_once(__DIR__ . '/lib/setup.php');" >> config.php

cd /etc/php/8.1/apache2
sudo sed -e '/upload_max_filesize/ s/^#*/#/' -i php.ini
sudo sed -e '/post_max_size/ s/^#*/#/' -i php.ini
sudo sed -e '/max_execution_time/ s/^#*/#/' -i php.ini

sudo echo "upload_max_filesize = 150M" >> php.ini
sudo echo "post_max_size = 150M" >> php.ini
sudo echo "max_execution_time = 300" >> php.ini
sudo echo "max_input_vars = 5000" >> php.ini

sudo service apache2 restart
